#pragma once
#include <vector>
#include <string>

class cPath
{
public:
	cPath();
	cPath(std::string mapPoint);
	~cPath();
	int pathLength;
	std::vector<std::string> pathNodes;
};

