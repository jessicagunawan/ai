#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "cMap.h"
#include "cMapCell.h"

class cMapLoader
{
public:
	cMapLoader();
	cMapLoader(std::string filePath);
	~cMapLoader();
	cMap* getMap();
	
	
private:
	void loadMapFromFile(std::string filePath);
	cMap* map;
	
};

