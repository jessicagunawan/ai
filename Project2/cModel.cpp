#include "cModel.h"

cModel::cModel(char *path,bool loadColour)
{
	this->toLoadColour = loadColour;
	this->minXYZ = glm::vec3(9999999999.0f, 9999999999.0f, 9999999999.0f);
	this->maxXYZ = glm::vec3(-9999999999.0f, -9999999999.0f, -9999999999.0f);
	this->loadModel(path);
	}

cModel::cModel()
{
}


cModel::~cModel()
{
}

void cModel::DrawModel(int shaderId,glm::vec3 colour)
{
	for (unsigned int i = 0; i < meshes.size(); i++)
		meshes[i]->Draw(shaderId,colour);
}

void cModel::loadModel(std::string path)
{
	Assimp::Importer import;
	const aiScene *scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
		return;
	}
	this->directory = path.substr(0, path.find_last_of('/'));

	this->processNode(scene->mRootNode, scene);

	this->maxExtent = this->maxExtentXYZ.x;
	if (this->maxExtentXYZ.y > this->maxExtent) {
		this->maxExtent = this->maxExtentXYZ.y;
	}
	if (this->maxExtentXYZ.z > this->maxExtent) {
		this->maxExtent = this->maxExtentXYZ.z;
	}

	this->scaleForUnitBBox = 1.0f / this->maxExtent;
}


void cModel::processNode(aiNode *node, const aiScene *scene)
{
	// process all the node's meshes (if any)
	for (unsigned int i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(mesh, scene));
	}
	// then do the same for each of its children
	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}

void cModel::translateModel(glm::vec3 newPos) {
	for (int i = 0; i < this->meshes.size(); i++) {
		for (int j = 0; j < this->meshes[i]->vertices.size(); j++) {
			this->meshes[i]->vertices[j].Position.x += newPos.x;
			this->meshes[i]->vertices[j].Position.y += newPos.y;
			this->meshes[i]->vertices[j].Position.z += newPos.z;
		}
	}


}

cMesh* cModel::processMesh(aiMesh *mesh, const aiScene *scene)
{
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		// process vertex positions, normals and texture coordinates
		Vertex vertex;	
		glm::vec3 vector;
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;

		// Code to find the extents of the model
		// -------------------------------------
		if (vector.x < this->minXYZ.x) {
			this->minXYZ.x = vector.x;
		}
		if (vector.x > this->maxXYZ.x) {
			this->maxXYZ.x = vector.x;
		}
		if (vector.y < this->minXYZ.y) {
			this->minXYZ.y = vector.y;
		}
		if (vector.y > this->maxXYZ.y) {
			this->maxXYZ.y = vector.y;
		}
		if (vector.z < this->minXYZ.z) {
			this->minXYZ.z = vector.z;
		}
		if (vector.z > this->maxXYZ.z) {
			this->maxXYZ.z = vector.z;
		}

		this->maxExtentXYZ.x = this->maxXYZ.x - this->minXYZ.x;
		this->maxExtentXYZ.y = this->maxXYZ.x - this->minXYZ.x;
		this->maxExtentXYZ.z = this->maxXYZ.x - this->minXYZ.x;
		// -------------------------------------------
		// Model extents code ends

		vertex.Position = vector; 
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.Normal = vector;
		if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else {
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}

		vertices.push_back(vertex);
	}
	// process indices
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
		// process material
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<Texture> diffuseMaps = loadMaterialTextures(material,aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		std::vector<Texture> specularMaps = loadMaterialTextures(material,aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}

	return new cMesh(vertices, indices, textures,this->toLoadColour);
}


std::vector<Texture> cModel::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		bool skip = false;
		for (unsigned int j = 0; j < textures_loaded.size(); j++)
		{
			if (std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0)
			{
				textures.push_back(textures_loaded[j]);
				skip = true;
				break;
			}
		}
		if (!skip)
		{   // if texture hasn't been loaded already, load it
			Texture texture;
			texture.id = TextureFromFile(str.C_Str(), directory);
			texture.type = typeName;
			texture.path = str.C_Str();
			textures.push_back(texture);
			textures_loaded.push_back(texture); // add to loaded textures
		}
	}
	return textures;
}


void cModel::addSeparateTexturesToModel(std::string fileName, std::string directory, std::string typeName, cMesh* mesh){	//Add own texture
	// if texture hasn't been loaded already, load it
	std::vector<Texture> vecTex = mesh->getTexturesForMesh();
	Texture texture;
	texture.id = TextureFromFile(fileName.c_str(), directory);
	texture.type = typeName;
	texture.path = fileName.c_str();
	vecTex.push_back(texture);
	mesh->setTexturesForMesh(vecTex);
	//textures_loaded.push_back(texture);
}

unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma)
{
	std::string filename = std::string(path);
	filename = directory + '/' + filename;

	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}

void cModel::getModelExtents() {
	std::cout << "Model XYZ extents : " << this->maxExtentXYZ.x << "   ----  "<< this->maxExtentXYZ.y << "  -----  "<< this->maxExtentXYZ.z<< std::endl;
}

std::vector<cMesh*> cModel::getAllMeshes() {
	return this->meshes;
}