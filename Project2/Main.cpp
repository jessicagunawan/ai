// HelloTriangle.cpp : Defines the entry point for the console application.

#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "cShaderManager.h"
#include "stb_image.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "cCamera.h"
#include <string>
#include "cGameObject.h"
#include "Utilities.h"
#include "cMapLoader.h"

cShaderManager* gPShaderManager;
std::vector<cGameObject*> gGameObjects;
std::map<std::string,cModel*> gMapLoadedModels;	
glm::vec3 gGravity = glm::vec3(0.0f, -1.0f, 0.0f);
cMapLoader gMapLoader("map.txt");
cMap* ourMap = gMapLoader.getMap();


// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
cCamera camera(glm::vec3(20.0f, 10.0f, 5.0f),glm::vec3(0.0f,1.0f,0.0f), -180.0f, -30.0f);
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame

// lighting
glm::vec3 lightPos(0.0f, 10.0f, 100.0f);

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	//camera Movement code
	float cameraSpeed = 2.5f * deltaTime; // adjust accordingly as per the frame render rate
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

/*
	glfw: whenever the mouse moves, this callback is called
*/
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

/* 
	glfw: whenever the mouse scroll wheel scrolls, this callback is called
*/
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}


void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
		
	
	}
}
/*
	Function to split the string format of co-ordinates in the lastShortestPath
	variable.Every value is in the format (x,z). Based on the points , we calulate 
	their index and find the relevant position for our player to be.
*/
void getPlayerPositionAsPerPath(glm::vec3 &pos, std::string point, int maxColumns) {
	int *vecValues = new int[2];
	point = point.substr(1, point.length() - 2);
	std::string delimiter = ",";

	size_t last = 0;
	size_t next = 0;
	int index = 0;
	std::string token;
	while (index<2)
	{
		next = point.find(delimiter, last);
		token = point.substr(last, next - last);
		vecValues[index] = (std::stoi(token));
		last = next + 1;
		index++;
	}

	int pointIndex = (vecValues[0] * maxColumns) + vecValues[1];
	pos = gGameObjects[pointIndex]->position;
}


void animatePlayer(cGameObject* gameObject, int &pathIndex, float &time, float stepTime) {
	std::string getPoint = ourMap->lastShortestPath->pathNodes[pathIndex];
	glm::vec3 finalPos = glm::vec3(0.0f);
	getPlayerPositionAsPerPath(finalPos,getPoint, ourMap->getMapColumns());
	glm::vec3 playerPos = glm::vec3(0.0f);
	playerPos.x = ((finalPos.x - gameObject->position.x) * stepTime) + gameObject->position.x;
	playerPos.z = ((finalPos.z - gameObject->position.z) * stepTime) + gameObject->position.z;
	gameObject->position = playerPos;
	time = time - stepTime;
	if (time <= 0.0f) {
		time = 1.0f;
		if (pathIndex == ourMap->lastShortestPath->pathLength)
			gameObject->toAnimate = false;
		else
			pathIndex += 1;
	}
};

/*
	main function
*/
int main()
{
	//Initializing GLFW
	glfwInit();
	//Setting Major and minor Context versions for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//Setting the glfw for cor profile to use extra features
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	GLFWwindow* mainWindow = glfwCreateWindow(800, 600, "Camera", NULL, NULL);
	if (mainWindow == NULL) {
		std::cout << "Error while creating window : \n";
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(mainWindow);
	glfwSetFramebufferSizeCallback(mainWindow, framebuffer_size_callback);
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(mainWindow, mouse_callback);
	glfwSetScrollCallback(mainWindow, scroll_callback);
	glfwSetMouseButtonCallback(mainWindow, mouse_button_callback);

	//Load glad for gl commands
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	//Code to load shaders

	::gPShaderManager = new cShaderManager();
	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVertex.glsl";
	fragShader.fileName = "simpleFragment.glsl";

	::gPShaderManager->setBasePath("assets/shaders/");

	if (!::gPShaderManager->createProgramFromFile("myCustomShader", vertShader, fragShader)) {
		std::cout << " Error in loading shader " << std::endl;
		return -1;
	}

	glEnable(GL_DEPTH_TEST);
	
	GLint customShaderId = ::gPShaderManager->getIDFromFriendlyName("myCustomShader");
	::gPShaderManager->useShaderProgram(customShaderId);

	// load models
	// -----------
	cModel* floorModel = new cModel("assets/models/floor.obj",false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("floor", floorModel));
	std::cout << "\n Extents for floor models are : \n";
	floorModel->getModelExtents();
	std::cout << "\n";


	cModel* wallModel = new cModel("assets/models/wall.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("wall", wallModel));
	std::cout << "\n Extents for wall models are : \n";
	wallModel->getModelExtents();
	std::cout << "\n";

	cModel* playerModel = new cModel("assets/models/tbag/T-Bag.obj", false);
	gMapLoadedModels.insert(std::pair<std::string, cModel*>("player", playerModel));
	std::cout << "\n Extents for wall models are : \n";
	playerModel->getModelExtents();
	std::cout << "\n";

	std::vector<cMesh*> modelMeshes = floorModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		floorModel->addSeparateTexturesToModel("floor.jpg", "assets/textures", "texture_diffuse", modelMeshes[i]);
	}

	modelMeshes.clear();
	modelMeshes = wallModel->getAllMeshes();
	for (int i = 0; i < modelMeshes.size(); i++) {
		wallModel->addSeparateTexturesToModel("wall.bmp", "assets/textures", "texture_diffuse", modelMeshes[i]);
	}
	
		ourMap->showMap();
	ourMap->findShortestPathTillEnd();

	//system("pause");

	std::vector<cMapCell*> mapCells;
	ourMap->getMapCells(mapCells);


	int pathIndex = 0;
	glm::vec3 prevCellPos = glm::vec3(-(wallModel->maxExtentXYZ.x / 2), 0.0f,wallModel->maxExtentXYZ.z / 2);
	int rowId = 0, colId = 0;

	//----------------
	for (int i = 0; i < mapCells.size(); i++) {
		cGameObject* tmpGameObject = new cGameObject();
		glm::vec3 newPos = glm::vec3(1.0f, 1.0f, prevCellPos.z);
		if (mapCells[i]->isWalkable) {
			tmpGameObject->model = gMapLoadedModels["floor"];
			tmpGameObject->colour = glm::vec3(1.0f, 0.0f, 0.0f);
			newPos.y = 0.0f;
		}
			
		else{
			tmpGameObject->model = gMapLoadedModels["wall"];
			tmpGameObject->colour = glm::vec3(1.0f, 1.0f, 1.0f);
			newPos.y = wallModel->maxExtentXYZ.y / 2;
		}
		tmpGameObject->objectId = gGameObjects.size();
		if (mapCells[i]->rowId > rowId) {
			newPos.z = prevCellPos.z + 1;
			prevCellPos.x = -(tmpGameObject->model->maxExtentXYZ.x / 2);
			rowId = mapCells[i]->rowId;
		}
		newPos.x = prevCellPos.x + 1;
		prevCellPos = newPos;
		tmpGameObject->position = newPos;
		tmpGameObject->typeOfObject = eTypeOfObject::PLANE;
		gGameObjects.push_back(tmpGameObject);
	}
	
	{
		cGameObject* tmpGameObject = new cGameObject();
		tmpGameObject->model = gMapLoadedModels["player"];
		cGameObject* pOtherGameObject = gGameObjects[ourMap->startCellIndex];
		glm::vec3 newPos = pOtherGameObject->position;
		tmpGameObject->objectId = gGameObjects.size();
		tmpGameObject->colour = glm::vec3(0.0f, 0.0f, 1.0f);
		tmpGameObject->scale = 0.3;
		tmpGameObject->ownAxisOrientation = glm::vec3(0.0f, glm::radians(90.0f),0.0f);
		tmpGameObject->typeOfObject = eTypeOfObject::PLAYER;
		tmpGameObject->position = newPos;
		tmpGameObject->toAnimate = true;
		gGameObjects.push_back(tmpGameObject);
	}	

	float timeToAAnimateOneStep = 1.0f;
	float stepTime = timeToAAnimateOneStep/10 ;
	
	unsigned int borderWidthLoc = glGetUniformLocation(customShaderId, "border_width");
	glUniform1f(borderWidthLoc,0.1f);


	unsigned int aspectRatioLoc = glGetUniformLocation(customShaderId, "aspect");
	glUniform1f(aspectRatioLoc, SCR_WIDTH/SCR_HEIGHT);


	//Main Window Loop 
	while (!glfwWindowShouldClose(mainWindow))
	{
		//per-frame logic
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		//Call to check for input
		processInput(mainWindow);
			
		glClearColor(0.3f, 0.5f, 0.4f, 1.0f);
		//gl command to clear the color buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// get matrix's uniform location and set matrix
		::gPShaderManager->useShaderProgram(customShaderId);

		glm::mat4 view = glm::mat4(1.0f);
		view = camera.GetViewMatrix();


		glm::mat4 projection;
		projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);

		unsigned int viewLoc = glGetUniformLocation(customShaderId, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		unsigned int projectionLoc = glGetUniformLocation(customShaderId, "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

		for (int i = 0; i < gGameObjects.size(); i++) {

			glm::mat4 model = glm::mat4x4(1.0f);

			glm::mat4 matPreRotZ = glm::mat4x4(1.0f);
			matPreRotZ = glm::rotate(matPreRotZ, ::gGameObjects[i]->originOrientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
			model = model * matPreRotZ;

			glm::mat4 matPreRotY = glm::mat4x4(1.0f);
			matPreRotY = glm::rotate(matPreRotY, ::gGameObjects[i]->originOrientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			model = model * matPreRotY;

			glm::mat4 matPreRotX = glm::mat4x4(1.0f);
			matPreRotX = glm::rotate(matPreRotX, ::gGameObjects[i]->originOrientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			model = model * matPreRotX;

			glm::mat4 trans = glm::mat4x4(1.0f);
			trans = glm::translate(trans, ::gGameObjects[i]->position);
			model = model * trans;


			glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
			matPostRotZ = glm::rotate(matPostRotZ, ::gGameObjects[i]->ownAxisOrientation.z, glm::vec3(0.0f, 0.0f, 1.0f));
			model = model * matPostRotZ;

			glm::mat4 matPostRotY = glm::mat4x4(1.0f);
			matPostRotY = glm::rotate(matPostRotY, ::gGameObjects[i]->ownAxisOrientation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			model = model * matPostRotY;

			glm::mat4 matPostRotX = glm::mat4x4(1.0f);
			matPostRotX = glm::rotate(matPostRotX, ::gGameObjects[i]->ownAxisOrientation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			model = model * matPostRotX;

			float finalScale = ::gGameObjects[i]->scale;

			glm::mat4 matScale = glm::mat4x4(1.0f);
			matScale = glm::scale(matScale, glm::vec3(finalScale, finalScale, finalScale));
			model = model * matScale;

			unsigned int modelLoc = glGetUniformLocation(customShaderId, "model");
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

			if(gGameObjects[i]->isWireFrame)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

			gGameObjects[i]->model->DrawModel(customShaderId, gGameObjects[i]->colour);

			if(gGameObjects[i]->typeOfObject == eTypeOfObject::PLAYER && gGameObjects[i]->toAnimate)
				animatePlayer(gGameObjects[i],pathIndex, timeToAAnimateOneStep,stepTime);
		}		

		glfwSwapBuffers(mainWindow);
		glfwPollEvents();
	}
	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}