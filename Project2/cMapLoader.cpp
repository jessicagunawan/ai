#include "cMapLoader.h"


/*	
	Constructor whic takes string as an input and then calls loadMap from file.
*/
cMapLoader::cMapLoader(std::string filePath)
{
	this->map = new cMap();
	loadMapFromFile(filePath);
}


/*
	Parameterless Constructor
*/
cMapLoader::cMapLoader()
{

}

/*
	Parameterless Destructor
*/
cMapLoader::~cMapLoader()
{
}

/*
	Function to load config file from map.txt
*/

void cMapLoader::loadMapFromFile(std::string filePath){
	std::fstream infoFile(filePath.c_str());
	if (!infoFile.is_open()) {
		std::cout << "Error in reading the config file";
		return;
	}
	else {
		std::cout << "File read. Storing config now !" << std::endl;
		std::string strConfigValue;
		infoFile >> strConfigValue;
		std::cout << "Config Value :" + strConfigValue << std::endl;
		while (strConfigValue != "#ENDCONFIG#") {
			if (strConfigValue == "mapStartPoint") {
				std::string data;
				bool bKeepReading = true;
				std::stringstream tmpStringStream;
				do {
					infoFile >> strConfigValue;
					if (strConfigValue == ";") {
						bKeepReading = false;
						data = tmpStringStream.str();
					}
					else {
						tmpStringStream << strConfigValue;
					}
				} while (bKeepReading);
				this->map->setMapStartPoint(data);
				std::cout << "New title :" + tmpStringStream.str() << std::endl;
			}
			else if (strConfigValue == "mapEndPoint") {
				std::string data;
				bool bKeepReading = true;
				std::stringstream tmpStringStream;
				do {
					infoFile >> strConfigValue;
					if (strConfigValue == ";") {
						bKeepReading = false;
						data = tmpStringStream.str();
					}
					else {
						tmpStringStream << strConfigValue;
					}
				} while (bKeepReading);
				std::cout << "New title :" + tmpStringStream.str() << std::endl;
				this->map->setMapEndPoint(data);
			}
			else if (strConfigValue == "mapRows") {
				infoFile >> strConfigValue;
				this->map->setMapRows(std::stoi(strConfigValue));
				std::cout << "Map Rows :" << strConfigValue << std::endl;
			}
			else if (strConfigValue == "mapColumns") {
				infoFile >> strConfigValue;
				this->map->setMapColumns(std::stoi(strConfigValue));
				std::cout << "Map Columns :" << strConfigValue << std::endl;
			}
			else if (strConfigValue == "Map:") {
				std::vector<cMapCell*> mapCells;
				this->map->getMapCells(mapCells);
				for (int i = 0; i < this->map->getMapRows(); i++) {

					for (int j = 0; j < this->map->getMapColumns(); j++) {
						cMapCell* tmpCell = new cMapCell();
						tmpCell->rowId = i;
						tmpCell->columnId = j;
						infoFile >> strConfigValue;
						tmpCell->isWalkable = std::stoi(strConfigValue);
						mapCells.push_back(tmpCell);
					}
				}
				this->map->setMapCells(mapCells);
				this->map->setupStartAndEndPoints();
			}
			infoFile >> strConfigValue;
			std::cout << "Config Value :" + strConfigValue << std::endl;
		}
	}
								


}

/*
	Function to return the map object created when 
	the config was loaded.
*/
cMap* cMapLoader::getMap() {
	return this->map;
}