#include "cMesh.h"

/*
	Parameterless constructor
*/
cMesh::cMesh()
{
}

/*
	Parameterless destructor
*/
cMesh::~cMesh()
{
}


/*
	Constructor taking in vertices , textures and if colour is supposed to be loaded or not.
*/
cMesh::cMesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures,bool loadColour)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;
	this->toLoadColour = loadColour;
	this->setupMesh();

}

/*
	Function to setup the entire mesh in the VAO.
*/
void cMesh::setupMesh()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int),&indices[0], GL_DYNAMIC_DRAW);

	// vertex positions
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glEnableVertexAttribArray(0);
	// vertex normals
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	glEnableVertexAttribArray(1);
	// vertex texture coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
	glEnableVertexAttribArray(2);
	glBindVertexArray(0);
}


/*
	Function to draw each mesh and to bind the textures and the materials to the shader.
*/
void cMesh::Draw(GLint shaderId,glm::vec3 colour)
{
	unsigned int colourBoolLoc = glGetUniformLocation(shaderId, "isObjColour");
	glUniform1i(colourBoolLoc, this->toLoadColour);

	if (this->toLoadColour) {
		unsigned int colourLoc = glGetUniformLocation(shaderId, "objColour");
		glUniform3fv(colourLoc, 1, glm::value_ptr(glm::vec3(colour)));
	}

	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;

	for (unsigned int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); // activate proper texture unit before binding
										  // retrieve texture number (the N in diffuse_textureN)
		std::string number;
		std::string name = textures[i].type;
		if (name == "texture_diffuse")
			number = std::to_string(diffuseNr++);
		else if (name == "texture_specular")
			number = std::to_string(specularNr++);

		unsigned int materialTextureLoc = glGetUniformLocation(shaderId, ("material." + name + number).c_str());
		glUniform1i(materialTextureLoc, i);

		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}
	glActiveTexture(GL_TEXTURE0);

	// draw mesh
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}


void cMesh::setTexturesForMesh(std::vector<Texture> textures) {
	this->textures = textures;

};
std::vector<Texture> cMesh::getTexturesForMesh() {
	return this->textures;
};
