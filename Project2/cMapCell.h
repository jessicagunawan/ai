#pragma once
#include <vector>

class cMapCell
{
public:
	cMapCell();
	~cMapCell();
	int rowId;
	int columnId;
	bool isWalkable;
	int parentId;
	int f, g, h;
	void calculateFForAdjacentCells(std::vector<cMapCell*> &vecCells, int endIndex, std::vector<cMapCell*> &openCells, std::vector<cMapCell*> &closedCells, int maxColumns, int maxRows);
	void calculateF(cMapCell* endMapCell);
};

