#pragma once
#include "cMesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "stb_image.h"
unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma = false);

class cModel
{
public:
	/*  Functions   */
	cModel(char *path,bool loadColour);
	cModel();
	~cModel();
	void translateModel(glm::vec3 newPos);
	void DrawModel(GLint shaderId,glm::vec3 colour);
	void getModelExtents();
	glm::vec3 minXYZ;
	glm::vec3 maxXYZ;
	glm::vec3 maxExtentXYZ;
	std::vector<cMesh*> getAllMeshes();
	float maxExtent;
	float scaleForUnitBBox;
	void addSeparateTexturesToModel(std::string fileName, std::string directory, std::string typeName, cMesh* mesh);
private:
	/*  Model Data  */
	std::vector<cMesh*> meshes;
	std::string directory;
	std::vector<Texture> textures_loaded;
	/*  Functions   */
	void loadModel(std::string path);
	void processNode(aiNode *node, const aiScene *scene);
	cMesh* processMesh(aiMesh *mesh, const aiScene *scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type,std::string typeName);
	bool toLoadColour;
};