#pragma once
#include <iostream>
#include <vector>
#include "cMapCell.h"
#include <string>
#include "cPath.h"
class cMap
{
public:
	cMap();
	~cMap();
	void getMapCells(std::vector<cMapCell*> &map);
	void setMapCells(std::vector<cMapCell*> map);
	void setMapRows(int rows);
	void setMapColumns(int columns);
	int  getMapRows();
	int  getMapColumns();
	void setMapStartPoint(std::string startPoint);
	void setMapEndPoint(std::string endPoint);
	void setupStartAndEndPoints();
	void showMap();
	int startCellIndex, endCellIndex;
	void findShortestPathTillEnd();
	void parseOpenCells(std::vector<cMapCell*> &openCells, std::vector<cMapCell*> &closedCells);
	void getShortestPath();
	cPath* lastShortestPath;

private:
	std::vector<cMapCell*> vecCells;
	int mapRows;
	int mapColumns;
	void getCellIndexForPoints(int &pointIndex,std::string point);
	std::string mapStartPoint, mapEndPoint;
};

