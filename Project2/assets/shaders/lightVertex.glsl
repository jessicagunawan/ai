#version 330 core
layout (location = 0) in vec3 objPos;
//layout (location = 1) in vec3 objCol;
//layout (location = 1) in vec2 objTex;


//out vec3 objColor;
//out vec2 objTexture;

uniform mat4 transform;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main(){
	gl_Position = projection * view * model * vec4(objPos.x,objPos.y,objPos.z,1.0);
}