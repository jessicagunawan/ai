#version 330 core
layout (location = 0) in vec3 objPos;
layout (location = 1) in vec3 objCol;
layout (location = 2) in vec2 objTex;

out vec3 objColor;
out vec2 objTexture;

void main(){
	gl_Position = vec4(objPos.x,objPos.y,objPos.z,1.0);
	objColor = objCol;
	objTexture = objTex;
}